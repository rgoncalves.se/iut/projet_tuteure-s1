# Projet tuteuré de S1

## Pré-requis

### Gitlab

Gitlab nous permet de mettre en commun nos fichiers et notre code source.
Créer un compte sur <https://gitlab.com>, puis contacter Romain pour vous ajouter au repository privé.

Un client visuel est conseillé, tel que *Github Desktop* ou d'autres.

### Communication

La communication s'effectue par l'utilisation de l'application *Riot*, disponible sur toutes les plateformes et navigateurs web.

### Thème

Le thème est : la sécurité informatique chez les professionnels

<https://matrix.to/#/!xqjaJOnoNJyibLxkKa:matrix.org?via=matrix.org>

### Réunion

Prendre des notes pour le dates auquel il faut faire des envois.

### Interview

Interview à General Electric ?
Le fonctionnement est différent à une PME.

Interview au CRI ?

<https://docs.google.com/presentation/d/11C1zCkVkLHkl4neenLOo4lLNqK4t4WiaVOdKfRce8YY/edit?usp=sharing>
