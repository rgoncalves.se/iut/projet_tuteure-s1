---
title : Charte Graphique - PT-S1
date : 2019-11-07
theme : iutbm
header-includes:
- \usepackage{tcolorbox}
- \usepackage{xcolor}
---


## Couleurs

\definecolor{greybg}{HTML}{FAFAFA}
\definecolor{blackbg}{HTML}{1D2731}
\definecolor{bluebg}{HTML}{4484CE}
\definecolor{yellowbg}{HTML}{F9F932}


- Couleur d'arrière plan : #FAFAFA *(neutre, pureté, design)*

\begin{tcolorbox}[width=\textwidth,colback={greybg}]
\end{tcolorbox}

- Couleur de premier plan : #1D2731 *(contraste)*

\begin{tcolorbox}[width=\textwidth,colback={blackbg}]
\end{tcolorbox}

- Couleur d'accentuation : #4484CE *(fiabilité, sécurité)*

\begin{tcolorbox}[width=\textwidth,colback={bluebg}]
\end{tcolorbox}

- Couleur complémentaire : #F9F932 *(prudence, connaissance)*

\begin{tcolorbox}[width=\textwidth,colback={yellowbg}]
\end{tcolorbox}


## Polices

- Police de texte : Raleway
- Police de titre : Nunito (gras)
	- Stabilité

## Logo
