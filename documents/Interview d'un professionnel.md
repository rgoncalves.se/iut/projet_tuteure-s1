<img src="/home/othen/git/projet_tuteure_s1/documents/maxresdefault.jpg" style="zoom:25%;" />

# Interview d'un professionnel
### Frank BRIGNOLI: Directeur technique chez TradeMachines

































-------------------
### Par Evan DUBOIS et Romain GONCALVES

**Le 15 octobre 2019**









### Introduction 

​	Le 10 octobre 2019, j'ai eu la chance d'avoir été mis en contact avec Frank BRIGNOLI, dans le cadre de mes projets personnels et professionnels. Ce dernier étant installé à Berlin (Allemagne), il nous a été impossible de nous rencontrer pour cet entretien. Cependant, il a tout de même accepté de répondre à mes questions via internet. C'est alors que je me suis mis à réaliser un questionnaire auquel il a répondu ce mardi 15 octobre. Voici ses réponses:

## I. Présentation 

​	Frank BRIGNOLI, âgé de 35 ans, est français mais vit et travaille à Berlin en Allemagne. Ses passions et centres d'intérêts sont la lecture, le sport (tel que la course à pied ou le vélo), les jeux vidéos, ainsi que les startups et les innovations: passions qu'il entretient via son travail. En effet, depuis février 2014 dans la startup Allemande: TradeMachines, il a le rôle de directeur technique (CTO: Chief Technology Officer). 

​	TradeMachines est en réalité un moteur de recherche de machines industrielles d'occasions fondé en octobre 2013. Soit 4 mois avant l'intégration de Frank BRIGNOLI au sein de l'équipe où il tient un rôle majeur.

## II. Son rôle 

​	Pour Mr BRIGNOLI, sa profession de directeur technique, se définit de la manière suivante: "Aligner la technologie avec la vision du business". Il n'a donc pas un rôle unique mais bien plusieurs qu'il mène au mieux.

​	Le premier est le rôle stratégique. En effet, il participe activement à la définition des objectifs de la société afin d'assurer son bon fonctionnement au long terme et de garder le cap sur l'avenir. Il doit également définir les nouveaux projets techniques que va aborder l'entreprise pour atteindre ses objectifs. 
​Le second rôle se définit par la recherche et le développement.  Travaillant dans une entreprise basée sur l'informatique, il se doit d'être à l'écoute des nouveautés techniques qui pourraient aider la société. De plus, il réalise des prototypes afin d'analyser et d'étudier ces nouvelles technologies.
​Etant directeur technique, il a pour rôle la gestion d'équipe. Cela consiste en d'autres termes à structurer et à manager des employés afin d'en tirer le meilleur. Il doit donner la possibilité aux membres du groupe d'offrir leurs compétences les plus importantes pour l'avancer de la société.

​	Finalement, une de ses journée ressemble à cela: dès l'arriver, le matin à 9h00, lecture de mail, organisation de l'équipe et du planning. Durant toute la matinée il participera à plusieurs réunions tels que des entretiens d'embauches, des réunions d'équipes ou techniques voir même un comité de direction. Puis à 14h, il va s'intéresser aux nouvelles technologies et réaliser des prototypes lui permettant ainsi de coder toute l'après midi jusqu'à 18h. Si il en est là aujourd'hui, c'est bien en passant par plusieurs étapes importantes.

## III. Son parcours

​	Mr BRIGNOLI a obtenu un DUT (Diplôme Universitaire de Technologie) en informatique . Il a choisi cette formation car il s'agit d'études courtes et pragmatiques qui apportent un niveau technique important et donnant une bonne vue d'ensemble sur l'informatique. Son objectif était de devenir développeur,  mais sans vraiment penser à "l'après".  Il voulait avant tout permettre d'influencer l'évolution d'une entreprise, notamment grâce à la technologie.

​	"En tant que développeur, on a une idée assez précise des nouvelles technologies qui existent et ce qu'elles font" d'après lui. Son objectif était de donc de passer du temps à réfléchir à leurs possibles impacts sur une entreprise.

​	Par la suite, il a rejoint une grande ESN (Entreprise de Services du Numérique) lui permettant d'intégrer l'entreprise de TradeMachines en Février 2014. Au sein de cette société, il a commencé tout d'abord en tant que développeur puis, 1 an après, s'est retrouvé architecte et opérateur. A ce moment là, son activité consistait à structurer le code et les services, puis de s'assurer qu'il n'y avait pas de discontinuité de ce service. En 2016, il est passé Lead équipe où sa mission était de coder tout en permettant à l'équipe de travailler sans difficulté et sans accroc. C'est seulement 4 ans après avoir intégré TradMachines, soit en 2018, qu'il se retrouve directeur technique.

​	Aujourd'hui, il est très épanoui dans son travail. Cette profession satisfait totalement ses attentes.

## III. La sécurité de l'entreprise

​	Pour Frank BRIGNOLI, la sécurité informatique est très importante pour trois raisons principales protéger les assets et la fiabilité de leurs services, protéger la réputation de l'entreprise vis à vis des clients mais également face au autres concurrents et enfin protéger la vie privé de leurs utilisateurs. Ce dernier point est intéressant car TradeMachines, étant un  site internet proposant des échanges commerciaux, est de ce fait très intéressé par les informations de contacts (email, adresse, téléphone). Il est donc nécessaire de protéger toute ses données. TradeMachines récupère les données de plus de 1150 vendeurs ainsi que de plus de 520 000 visiteurs par mois. Leur protection se doit d'être irréprochable

​	Pour protéger toutes les données citées plus haut, TradeMachines mes en place différents moyens tel que des chiffrements de données sensibles, des suppressions de données automatiquement après un certain délai ou encore une restriction de l'accès aux données. Par la suite, il réalise également des audits de sécurité sur leurs serveurs et postes de travail. Cependant, il ne font pas appel à des sous traitant pour la maintenance informatique ou encore pour la gestion des données. En effet, tout cela est réaliser en interne par le soin des professionnels.

​	Les serveurs de TradeMachines sont sous traité dans le cloud. Cela leur permet de se concentrer sur le cœur du métier de leur startup. Mais également de bénéficier de services qu'ils ne pourraient pas avoir en interne sans y accorder un large budget tel que une sécurité physique des serveurs, de multiples data-center proches de leurs clients, une large offres de services (parmis eux : une base de données, des serveurs, un stockage de fichiers,…), une sécurité logicielle: comme des sauvegardes, des mises jours logiciels ou encore une sécurité réseau ou encore une grande flexibilité avec la possibilité de changer le nombre et le type de serveurs en un seul click.

​	Le **RGPD** ou autrement dit le règlement général  sur la protection des données, est un règlement de l'Union européenne  qui constitue le texte de référence en matière de protection des données à caractère personnel. Il renforce et unifie la protection des données pour les individus au sein de l'Union européenne. Il est entrée en vigueur le 14 avril 2016.

​	Dans le cas de TradeMachines, n'ayant jamais eu de problèmes juridique quel qu'il soit, la venu du RGPD ne les a pas éffrayé. En effet leur réaction a été de nommé un représentant interne  afin d'analyser les changements nécessaires à réaliser. On peut citer par exemple les changements dans leur méthode de  communication envers leurs utilisateurs (cookies, conservation des  données,...) ou encore dans leur rétention des données.

 ### Conclusion

​	
