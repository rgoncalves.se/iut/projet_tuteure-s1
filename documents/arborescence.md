src
├── img
│   ├── banner_chiffrement_01.jpg
│   ├── banner_cpu_01.jpg
│   ├── banner_datacenter_01.jpg
│   ├── favicon.ico
│   ├── icon_iutbm.png
│   ├── logo_anssi.png
│   ├── logo_cnil.png
│   ├── logo_debian.png
│   └── ...
├── index.php
├── js
│   ├── collapse.js
│   ├── jquery-1.11.0.min.js
│   └── magneticScroll-1.0.min.js
├── php
│   ├── actualites.php
│   ├── contact.php
│   ├── _footer.php
│   ├── interview.php
│   ├── _navbar.php
│   └── presentation.php
├── README.md
└── style
    ├── style_base.css
    ├── style.css
    ├── style_footer.css
    ├── style_navbar.css
    └── style_pages.css
