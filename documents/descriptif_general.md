# Descriptif Général

Nous souhaitons présenter la sécurité informatique chez les professionnels :

- A travers une introduction, présenter l'importance de la sécurité informatique à l'aide de cas concrets.
- A l'aide d'une présentation générale, expliquer ce qu'est la sécurité informatique chez les professionnels, et aborder le coté juridique du domaine.
- A l'aide de 2 interviews de personnes travaillant dans ce domaine, afin d'affiner la présentation général en donnant des exemples concrets.
- Parler de l'actualité qu'il y a autour de ce domaine, et faire de la prévention.
