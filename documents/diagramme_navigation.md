## Diagramme de Navigation

```mermaid
graph LR

id1:(("Racine")) --- id2:("Html")
id2:(("Html")) --- actualites.html
id2:(("Html")) --- presentation.html
id2:(("Html")) --- interview.html
id2:(("Html")) --- ...

id1:(("Racine")) --- id3:(("Php"))
id1:(("Racine")) --- id4:(("images"))
id1:(("Racine")) --- id5:("Style")

id1:(("Racine")) --- index.html

id5:(("style")) --- id6:["style.css"]
```



