# Cahier des charges 

## I. Contenu général 

```mermaid
graph TD
id1:(("Accueil")) --> id2:("Interview")
id1:(("Accueil")) --> id3:("Présentation")
id1:(("Accueil")) --> id4:("Contact")
id1:(("Accueil")) --> id5:("Actualité")
```

### 1. Page d'accueil 

```mermaid
graph LR
id1:(("Accueil")) --> Sommaire  
Sommaire --> id2:("Interview")
Sommaire --> id3:("Présentation")
Sommaire --> id4:("etc..")
id1:(("Accueil")) --> footer
```

