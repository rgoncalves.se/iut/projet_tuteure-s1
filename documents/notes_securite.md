# Notes : Sécurité

La sécurité d'un système informatisé à pour but de protéger les données contenues dans ce dernier.

En effet, le système de l'information est aujourd'hui un élément crucial des entreprises et des institutions.
A travers les paragraphes suivant, nous expliquerons pourquoi tout ce qui menace le S.I. menace directement l'entreprise.

## La sécurité physique

Le *système d'information* d'une entreprise, contenant les données importantes, se doit-être physiquement accessible qu'au personnel autorisé.
En effet, même déconnecté de tout réseau, toute information contenue peut-être la cible d'attaque via différents moyens :

- Clé usb
- Keylogger
- Vol des disques durs

## La sécurité du microprocesseur

Dans un ordinateur, le microprocesseur est l'unité de calcul servant à traiter l'information.
Cette unité, composér de milliards de transistors, est celle qui traite l'information au plus bas niveau.

Ainsi, l'existence d'une faille dans un composant électronique est à traiter avec un tout autre degré.
En effet, le  matériel est dès lors rendu obsolète, bien que des mise à jour du système d'exploitation peuvent partiellement combler la faille 

Récemment, c'est dans les processeurs *Intel* que des failles ont étés révélées.
Elles permettent entre autre de bénéficier d'une escalation des privilèges et ainsi d'accéder au contenu de la mémoire système.

<https://meltdownattack.com/>

## La sécurité du système d'exploitation

Le *système d'exploitation* est le logiciel permettant de faire le pont entre le matériel et l'interface utilisateur.
Sa sécurité est principalement géré par *l'amdministrateur système*, qui s'assure des logiciels exécutés, des comportements de utilisateurs, et des historiques, ...

## La sécurité du réseau

Il faut s'assurer que les informations transmises à travers le réseau soient seulement lisibles par : l'émetteur et le récepteur.
Deux protocoles sont employés. le *TLS* et jusqu'à récemment le *SSL*, tout deux majoritairement utilisés via le logiciel *OpenSSL*.

Dans le cas d'un réseau local, chaque périphérique (ordinateurs, smartphones, tablettes, ...) rentrant et sortant quotidiennement du réseau peut présenter des risques.
C'est pourquoi dans les entreprises à risque, les adminstrateurs mettent en place des règles d'accès, interdisant ou restreignant l'accès à certains périphériques externes.
Par exemple, la mise en place d'un réseau wifi non-connecté à l'intranet de l'entreprise.
