# Diagramme de Gantt 

```mermaid
gantt
	dateFormat YYYY-MM-DD
	dateFormat MM-DD
	title Diagramme projet
	
	section Projet
		Descriptif générale:done, 2019-10-03, 2019-10-04
		Gabarit ecran:active, 2019-10-03, 2019-10-11
		DiagramGantt:active, 2019-10-03, 2019-10-11
		Diagram Navigation:active, 2019-10-03, 2019-10-11
		Recherche info: 2019-10-11, 2019-12-06
		Création du site:crit, 2019-10-11, 2019-12-09
		Interview 1: 2019-10-04, 2019-10-11
		Interview 2: 2019-10-11, 2019-10-25
		Maquette: 2019-10-25, 2019-11-08
		Descriptif générale:done, 10-03, 10-04
		Gabarit ecran:active, 10-03, 10-11
		DiagramGantt:active, 10-03, 10-11
		Diagram Navigation:active, 10-03, 10-11
		Recherche info: 10-11, 12-06
		Création du site:crit, 10-11, 12-09
		Accueil: 10-20, 11-10
		Header: 10-20, 11-03
		Footer: 10-27, 11-10
		Style CSS: 10-27, 12-01
		Autres pages: 11-03, 12-01
		PHP mailer: 11-24, 12-08
		Interview 1: 10-04, 10-11
		Interview 2: 10-11, 10-25
		Maquette: 10-25, 11-08
		Conception plan rapport: 2019-11-08, 2019-11-15
		Eco/Droit: 2019-11-08, 2019-11-15
		Intro rapport: 2019-11-15, 2019-11-25
		Rapport:crit, 2019-11-08, 2019-12-06
		Entraînement oral: 2019-12-06, 2019-12-20
		Eco/Droit: 11-08, 11-15
		Intro rapport: 11-15, 11-25
		Rapport:crit, 11-08, 12-06
		Entraînement oral: 12-06, 12-20
```

