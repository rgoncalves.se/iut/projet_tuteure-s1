# Utiliser ce site web :

## Pré-requis

Une connexion internet est requises afin d'avoir un affichage correcte des polices.

## Dépendences

- Apache/Nginx
- php

## Mise en place

La racine de ce site web, où se trouve le fichier *index.php*, doit se trouver à la racine d'un hôte virtuel, soit :

- 127.0.0.1:80
- 127.0.0.1:81
- 127.0.0.1:8080
