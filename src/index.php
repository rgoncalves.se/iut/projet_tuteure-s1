<!DOCTYPE html>
<html lang="fr" dir="ltr">
	<head>
		<meta charset="utf-8">
		<title>La sécurité informatique</title>
		<link href="style/style.css" rel="stylesheet" type="text/css" />
		<link rel="shortcut icon" type="image/png" href="img//favicon.ico"/>
		<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
		<script type="text/javascript" src="js/magneticScroll-1.0.min.js"></script>
		<script>
			$.magneticScroll();
		</script>
	</head>
	<body class="index">
		<section id="dAccueil">
			<div class="onepage magnetic">
				<?php include "php/_navbar.php" ?>
				<h1>La Sécurité Informatique</h1>
				<div class="preview no-border">
					<blockquote class="citation">"L'Homme et sa sécurité doivent constituer la première préoccupation de toute aventure technologique"
						<br>
						<b>A.Einstein</b>
					</blockquote>
				</div>
			</div>
		</section>
		<section id="dPresentation">
			<div class="onepage magnetic">
				<h2>Présentation</h2>
				<h3>de la sécurité informatique</h3>
				<div class="preview">
					<p>
					Nous sommes un groupe de 5 cinq personnes de l'IUT d'Informatique de Belfort-Montbéliard.
					</p>
					<p>
					Retrouvez ici notre présentation de la sécurité informatique, ainsi que les différentes recherches effectuées.
					</p>
					<a class="button" href="php/presentation.php">En savoir plus</a>
				</div>
			</div>
		</section>
		<section id="dJuridique">
			<div class="onepage magnetic">
				<h2>Enjeux juridiques</h2>
				<h3>et économiques</h3>
				<div class="preview">
					<p>
					Retrouvez ici les différents enjeux juridiques et économiques de la sécurité informatique actuelle.
					</p>
					<a class="button" href="php/juridique.php">En savoir plus</a>
				</div>
			</div>
		</section>
		<section id="dInterview">
			<div class="onepage magnetic">
				<h2>Interview</h2>
				<h3>avec un professionel</h3>
				<div class="preview">
					<p>
					Retrouvez ici une interview réalisée avec un professionnel, qui témoigne de sa façon de protéger informatiquement son entreprise.
					Il s'exprime à propos de la protection des données de l'entreprise ainsi que celles de ses utilisateurs.
					</p>
					<a class="button" href="php/interview.php">En savoir plus</a>
				</div>
			</div>
		</section>
		<section id="dActualites">
			<div class="onepage magnetic">
				<h2>Actualités</h2>
				<h3>récentes et faits marquants</h3>
				<div class="preview">
					<p>
					La sécurité est de plus en plus importante dans notre monde.
					En effet, de plus en plus d'objets sont connectés à internet.
					Retrouvez ici les dernières actualités ainsi que les moments historiques marquant le monde informatique.
					</p>
					<a class="button" href="php/actualites.php">En savoir plus</a>
				</div>
			</div>
		</section>
		<section id="dContact">
			<div class="onepage magnetic">
				<h2>Nous contacter</h2>
				<div class="preview">
					<p>
					Nous vous remercions d'avoir consulté notre site.
					Si vous souhaitez avoir plus d'informations, ou si vous avez des questions, veuillez s'il vous plait nous contacter via notre formulaire.
					</p>
					<a class="button" href="php/contact.php">Nous contacter ici</a>
				</div>
			</div>
		</section>
	</body>
	<?php include "php/_footer.php" ?>
</html>
