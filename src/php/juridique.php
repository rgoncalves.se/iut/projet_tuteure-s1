<!DOCTYPE html>
<html lang="fr" dir="ltr">
	<head>
		<meta charset="utf-8">
		<title>La sécurité informatique - Juridiction et économie</title>
		<link href="/style/style_base.css" rel="stylesheet" type="text/css" />
		<link href="/style/style_pages.css" rel="stylesheet" type="text/css" />
		<link rel="shortcut icon" type="image/png" href="/img/favicon.ico"/>
	</head>
	<?php include "_navbar.php" ?>
	<body>
		<div class="container">
			<section>
				<h1>Les enjeux juridiques et économiques de la sécurité informatique</h1>
				<hr class="title">
				<h2>Les enjeux juridiques</h2>
				<p>Afin d’évoquer les enjeux juridiques de la <strong>sécurité informatique</strong>, il nous est apparu essentiel de présenter les différentes lois qui encadrent les traitements de données automatisés dans le monde professionnel. Tout d’abord, nous découvrirons en détails le <strong>RGPD</strong>. Ensuite, nous nous pencherons sur la <strong>Loi Informatique et Libertés</strong>. Puis nous évoquerons rapidement les <strong>CGU</strong> des sites internet, et pour finir, les sanctions visant les cybercriminels en vigueur actuellement.</p>
			</section>
			<hr class="title">
			<section>
				<h3>Le RGPD</h3>
				<div class="content">
					<p>Afin d’établir les premières bases nécessaires à la compréhension des différents enjeux juridiques de la sécurité informatique, une présentation complète sur une loi bien précise de l’Union Européenne s’impose : celle du <strong>RGPD</strong>.</p>
					<p>Le <strong><a class="link" target="_blank" href="https://www.economie.gouv.fr/entreprises/reglement-general-sur-protection-des-donnees-rgpd">RGPD</a></strong> (<em>Règlement Général sur la Protection des Données</em>) ou <strong>GDPR</strong> (<em>General Data Protection Regulation</em>) en anglais, est une toute nouvelle loi européenne, entrée en application le 25 mai 2018 qui régit le traitement des données personnelles dont disposent les entreprises sur leurs clients pour leur proposer des services et des produits. Cette loi fait suite à une première directive datant de 1995, mais nous ne nous y intéresserons pas plus que ça car elle n’est plus d’actualité.</p>
					<p>Cette grande réforme de la protection des données n’impacte pas seulement les professionnels mais aussi les particuliers dont les données sont exploitées. Elle a notamment pour but de :</p>
					<ul>
						<li><strong>Renforcer les droits des personnes</strong></li>
						<li><strong>Responsabiliser les acteurs traitant des données</strong> (responsables de traitement et sous-traitants)</li>
						<li><strong>Crédibiliser les transferts de données transnationaux</strong> (hors de l’Union Européenne)</li>
					</ul>
					<img class ="img-banner" alt="Logo RGPD" src="../img/logo_rgpd.png">
					<section>
						<h4>Renforcer les droits des personnes</h4>
						<p>En effet, auparavant, les utilisateurs / clients n’avaient <strong>aucun accès à leurs données</strong> et étaient dans l’incapacité de contrôler leur collecte. Désormais, il existe un droit à la portabilité des données personnelles, qui signifie qu’un utilisateur doit pouvoir, sur demande, accéder aux données qu’elle a fournies. Dans cette configuration, les personnes récupèrent donc la maîtrise de leurs informations confidentielles. Des conditions particulières s’appliquent également aux mineurs : en effet, chaque pays établit un âge sous lequel la collecte de données doit être acceptée <strong>par l’enfant</strong> mais aussi <strong>par son tuteur</strong> (autorisation parentale).</p>
					</section>
					<section>
						<h4>Responsabiliser les acteurs traitant des données</h4>
						<p>Ces professionnels en entreprise doivent obligatoirement mettre en place des <strong>mesures techniques et organisationnelles</strong> de protection de données <strong>efficaces et appropriées</strong>. Ils doivent par ailleurs respecter de nombreuses autres règles, comme par exemple notifier la présence d’une éventuelle faille de sécurité aux autorités et aux personnes concernées. Si des données ont été dérobées, les utilisateurs doivent être informés de la situation dans les 72h après le début de l’incident.</p>
					</section>
					<section>
						<h4>Crédibiliser les transferts de données transnationaux</h4>
						<p>Les responsables de traitement sont forcés à utiliser un <strong>système de protection suffisant</strong> s’ils souhaitent transférer des données à l’international, sinon cette action leur est impossible. De plus, ces données transférées restent soumises au règlement européen.</p>
					</section>
				</div>
			</section>
			<hr class="title">
			<section>
				<h3>La Loi Informatique et Libertés</h3>
				<div class="content">
					<p>La <strong><a class="link" target="_blank" href="https://www.cnil.fr/fr/la-loi-informatique-et-libertes">Loi Informatique et Libertés</a></strong> est une loi éditée le 6 janvier 1978 en France ayant pour objectif de garantir la protection de la vie privée des citoyens face à l’informatique et aux traitements de données numériques. Elle a été réadaptée pour l’Union Européenne le 6 août 2004. Ce décret vise à éviter les <strong>utilisations</strong> malveillantes du traitement automatisé de données confidentielles, tels que le tri des juifs effectué par les systèmes d’information d’IBM du régime nazi allemand qui stockaient toutes les données collectées au recensement, dont la religion. L’informatique avait donc assisté le génocide juif de 1941 à 1945, lors de la Seconde Guerre mondiale.</p>
					<p>Cette loi est au final très ressemblante à celle du <strong>RGPD</strong>, mais elle est comme indiquée bien plus ancienne et a donc été à la base des premières réglementations sur les collectes de données personnelles et confidentielles sur les personnes. Son article 1 affirme par exemple que « L’informatique […] ne doit porter atteinte ni à l’identité humaine, ni aux droits de l’homme, ni à la vie privée, ni aux libertés individuelles et publiques ». En somme, il est parfaitement autorisé de stocker des données, mais elle ne doivent pas <strong>entraver la liberté</strong> de l’individu concerné.</p>
					<img class="img-banner" alt="Logo CNIL" src="../img/logo_cnil.svg">
				</div>
			</section>
			<hr class="title">
			<section>
				<h3>Les CGU</h3>
				<div class="content">
					<p>Beaucoup d’utilisateurs d’internet ne sont même pas au courant du fait que leurs données sont récoltées à des fins parfois (et très souvent) commerciales. Cela s’explique en partie par le fait que les <em>Conditions Générales d’Utilisation</em> (<strong><a class="link" target="_blank" href="https://fr.wikipedia.org/wiki/Conditions_g%C3%A9n%C3%A9rales_d%27utilisation">CGU</a></strong>) dont l’utilisateur doit, en théorie, prendre connaissance et accepter par la suite, sont en réalité lues par une infime partie de ces personnes. Les <strong>CGU</strong> sont très souvent jugés comme un texte beaucoup trop long s’étalant sur une page qui semble ne plus avoir de fin, et semble inutile à première vue. Or, toutes les informations juridiques que les entreprises sont en obligation de présenter à leurs clients, se trouvent dans ce document ; par conséquent, il est réellement nécessaire de prendre connaissance de toutes ces conditions. Malgré tout, avez-vous eu déjà souvenir d’avoir lu, en entier, les Conditions Générales d’Utilisation d’un quelconque site ? Il est très peu probable que cela soit déjà arrivé. On s’imagine donc bien le danger que peut présenter ce type de situation.</p>
					<img class="img-banner" alt="CGU" src="../img/cgu.jpg">
				</div>
			</section>
			<hr class="title">
			<section>
				<h2>Les enjeux économiques</h2>
				<p>Les enjeux économiques qui concernent la <strong>sécurité informatique</strong> actuellement sont tels qu’il est absolument nécessaire de les évoquer dans le cadre de ce projet tuteuré. Nous avons donc décidé de mêler juridiction et économie dans celui-ci. Nous allons donc tenter de percevoir comment des cyberattaques peuvent affecter, parfois très gravement, l’économie d’une entreprise, en donnant quelques exemples.</p>
			</section>
			<hr class="title">
			<section>
				<h3>Des attaques aux lourdes conséquences financières</h3>
				<div class="content">
					<p>Avant toute chose, il s’avère utile de présenter la première menace auxquelles les entreprises sont confrontées : le <strong>ransomware</strong>.</p>
					<p>Le <strong><a class="link" target="_blank" href="https://fr.wikipedia.org/wiki/Ran%C3%A7ongiciel">ransomware</a></strong> (logiciel de rançon en français), est un type de virus informatique (logiciel malveillant) très répandu, élaboré et transféré par des hackers. Le ransomware se propage généralement par mail. L’utilisateur du parc informatique d’une entreprise découvre un <strong>mail</strong> contenant un <strong>lien</strong>, et clique sur celui-ci sans savoir que cela peut avoir de graves conséquences. En effet, c’est à ce moment-là que le logiciel malveillant s’installe, à l’insu de l’utilisateur, et crypte toutes les données du système d’information en question. A ce moment-là, <strong>tous les ordinateurs</strong> deviennent donc <strong>inutilisables</strong>, sauf si une <strong>rançon</strong> demandée par le hacker, lui est versé en échange de la clé de déchiffrage. Un chiffre très parlant pour réaliser au combien ce type d’attaque devient de plus en plus répandu partout dans le monde : une augmentation du nombre de ransomware de <strong>36%</strong> a été notée entre <strong>2016</strong> et <strong>2017</strong>, et ce chiffre ne cesse d’accroître. Ils sont donc désormais la priorité absolue concernant la sécurité informatique mondiale</p>
					<p>Les <strong>administrations</strong> peuvent être particulièrement touchées par ces problèmes. En effet, depuis la révolution numérique, toutes les tâches administratives sont informatisées, dans les mairies par exemple. Nous avons vu que certaines mairies, après avoir été touchées par un <strong>ransomware</strong> et voyant donc leur système d’information temporairement bloqué, sont repassés au format papier momentanément, car elles ne peuvent pas se permettre de payer la rançon demandée par les cybercriminels. On comprend bien que cet évènement perturbe grandement le travail des fonctionnaires, et <strong>ralentit</strong> donc beaucoup de <strong>procédures</strong>. Quand on sait également toutes les données que peut comporter de telles structures… Il est évident que ces attaques ont un <strong>impact économique non négligeable</strong>.</p>
					<p>Mais ces attaques peuvent également toucher le <strong>service publique</strong>, et nous avons par exemple découvert que de nombreux <storng>hôpitaux</storng> sont régulièrement pris pour cible par les cybercriminels. Après s’être infiltrés dans le réseau informatique de l’hôpital en question, les hackers, grâce à un <strong>ransomware</strong> vu précédemment, bloquent le fonctionnement de nombreux appareils de santé connectés, avant de demander une rançon en échange du déblocage. Manifestement, la santé nous semble un domaine très facile à atteindre, où les rançons sont généralement acceptées par l’équipe de direction. Nous comprenons bien sûr que la <strong>santé des patients</strong> est immédiatement <strong>mise en jeu</strong> lors d’un dysfonctionnement total des appareils électroniques connectés. Les cybercriminels sont de vrais professionnels et savent où et comment frapper fort, par conséquent ils peuvent exiger des rançons exorbitantes en s’attaquant à ce type de victime. De plus, les <strong>problèmes financiers initiaux</strong> auxquels sont confrontés les services de santé rendent le <strong>recrutement</strong> d’une équipe d’experts en sécurité informatique parfois <strong>impossible</strong>, et ces évènements deviennent donc de réelles impasses, alors que des décisions aussi importantes que celles de payer une rançon ou non doivent être prises dans les plus brefs délais.</p>
				</div>
			</section>
			<hr class="title">
		</div>
	</body>
	<?php include "_footer.php"; ?>
</html>