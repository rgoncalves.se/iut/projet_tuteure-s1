<link href="/style/style_navbar.css" rel="stylesheet" type="text/css" />
<header>
	<nav>
	<?php
		if (strpos($_SERVER['SCRIPT_NAME'], 'index.php')) {
			echo '<a class="link-navbar" href="#dPresentation">Présentation</a>
			<a class="link-navbar" href="#dJuridique">Juridiction / Economie</a>
			<a class="link-navbar" href="#dInterview">Interview</a>
			<a class="link-navbar" href="#dActualites">Actualités</a>
			<a class="link-navbar" href="#dContact">Contact</a>';
		}
		else {
			echo '
			<div id="div-logo-navbar">
			<a id="link-logo-navbar" href="/index.php">
				<img id="logo-navbar" src="/img/logo.svg" alt="Logo Site" />
			</a>
			</div>
			<a class="link-navbar" href="presentation.php">Présentation</a>
			<a class="link-navbar" href="#dJuridique">Juridiction / Economie</a>
			<a class="link-navbar" href="interview.php">Interview</a>
			<a class="link-navbar" href="actualites.php">Actualités</a>
			<a class="link-navbar" href="contact.php">Contact</a>';
		}
	?>
	</nav>
</header>
