<!DOCTYPE html>
<html lang="fr" dir="ltr">
	<head>
		<meta charset="utf-8">
		<title>La sécurité informatique - Présentation</title>
		<link href="/style/style_base.css" rel="stylesheet" type="text/css" />
		<link href="/style/style_pages.css" rel="stylesheet" type="text/css" />
		<link rel="shortcut icon" type="image/png" href="/img/favicon.ico"/>
	</head>
	<?php include "_navbar.php" ?>
	<body>
		<div class="container">
			<section>
				<h1>Qu'est ce que la sécurité informatique ?</h1>
				<p>La <em>sécurité des systèmes d'informations</em>, plus souvent appelée <strong>sécurité informatique</strong>, ou encore <em>cybersécurité</em>, désigne l'ensemble des moyens permettant de sécuriser l'accès au appareils et données informatiques.</p>
				<p>En effet, la sécurité informatique est devenue ces vingt dernières années un élément crucial dans le fonctionnement des entreprises et de nos institutions.
				C'est pourquoi tout ce qui menace le système d'information menace directement son propriétaire, et peut conduire à la faillite de ce dernier.</p>
			</section>
			<hr class="title">
			<section>
				<h2>Les objectifs de la sécurité informatique</h2>
				<div class="content">
					<p>La sécurité informatique répond à différents objectifs :</p>
					<ul>
						<li><strong>La confidentialité</strong> des données</li>
						<li><strong>L'authentification</strong> vérifiée des utilisateurs</li>
						<li><strong>L'intégrité</strong> des données lors du stockages ou du déplacement de données</li>
						<li><strong>La disponibilité</strong> des services</li>
					</ul>
					<section>
						<h3>La confidentialité</h3>
						<p>Lors du traitement et du stockage des données, il faut s'assurer que seulement les utilisateurs, programmes et services autorisés puissent avoir accès aux données.</p>
					</section>
					<section>
						<h3>L'authentification</h3>
						<p>Lors de la connexion des utilisateurs au services, il faut s'assurer que l'utilisateur se connectant est réellement celui qui en fait la demande.
						Pour cela, différent moyens peuvent être mis en oeuvre :</p>
						<ul>
							<li><strong>Le mot de passe</strong> : quelque chose que l'utilisateur <strong>connaît</strong>.</li>
							<li><strong>La biométrie</strong> : quelque chose que l'utilisateur <strong>est</strong>.</li>
							<li><strong>La clé d'authentification</strong> : quelque chose que l'utilisateur <strong>possède</strong>.
								Cette dernière est sous forme physique (clé usb) et contient une 
								<a class="link" target="_blank" href="https://fr.wikipedia.org/wiki/Cryptographie_asym%C3%A9trique#Principe_g%C3%A9n%C3%A9ral">clé privée</a> de chiffrement.</li>
						</ul>
						<img class="img-banner" alt="Verrou" src="../img/banner_lock_01.jpg" />
					</section>
					<section>
						<h3>L'intégrité</h3>
						<p>L'intégrité des fichiers revient à savoir si le fichier <strong>source</strong> est le même que le fichier obtenu à la <strong>réception</strong>.
						De même lors du stockage des données, on souhaite que les fichiers ne soient pas altérés, et s'ils le sont, il faut pouvoir le savoir.</p>
						<p>Pour cela, on utilise la technologie de checksum ou de hachage.
						Des logiciels comme <a class="link" target="_blank" href="https://fr.wikipedia.org/wiki/GNU_Privacy_Guard">GPG</a> sont utilisés.
						Voici à quoi peut ressembler le hash et le sum d'un fichier quelconque, depuis une console <strong>UNIX</strong> :</p>
						<code>
							echo "projettuteure" | md5sum<br>
							fdb1eaa6f7fceaac0b64c1c2003038b0
							<hr>
							echo "projettuteure" | sha256sum<br>
							17cf2306d32facfb322c27bd1372eae6d687f043eb02c7d0ac5866377a1e32bf
						</code>
						<p>Ainsi, si <strong>l'empreinte</strong> du fichier (une des suites de chiffres et de lettres ci-dessus) du fichier source ne correspond au fichier reçu,
						il est alors évident que les deux fichiers <strong>sont différents</strong> l'un de l'autre.</p>
					</section>
					<section>
						<h3>La disponibilité</h3>
						<p>Les données mises à disposition par l'entreprise ou l'organisme doivent sans cesse être disponibles,
						qu'elles se trouvent sur les ordinateurs des utilisateurs, ou biens sur les serveurs.</p>
						<img class="img-banner" alt="Câbles réseaux connectant des serveurs" src="../img/banner_server_01.jpg" />
						<p>L'accès aux ressources doit se faire rapidement, même en situation de crise.
						Une solution courante est de mettre en place un système de sauvegardes de données : </p>
						<ul>
							<li>Cela revient à bénéficier de <strong>serveurs mirroirs</strong> : c'est à dire, avoir à disposition différents serveurs, possédant le même contenu, et étant disposés à des emplacements différents.</li>
						</ul>
					</section>
				</div>
			</section>
			<hr class="title">
			<section>
				<h2>Les différentes mesure de sécurité</h2>
				<div class="content">
					<section>
						<h3>La sécurité physique</h3>
						<p>Le système d'information se doit en premier lieu de n'être accessible physiquement qu'au personnel autorisé.
						Par exemple, cela peut s'effectuer par une salle à accès restreinte où seulement le personnel qualifié <em>-et muni d'un moyen d'authentification, tel qu'un badge-</em> peut y entrer.</p>
						<p>Cette restriction au niveau physique est d'autant plus importante que différents moyens d'actions peuvent être employés physiquement pour nuire au système d'information :</p>
						<ul>
							<li>Insertion d'une clé usb <strong>infectée</strong>, pouvant contenir des logiciels malveillants.</li>
							<li>Mise en place d'un <a class="link" target="_blank" href="https://fr.wikipedia.org/wiki/Enregistreur_de_frappe">keylogger</a>, 
								appareil permettant d'<strong>enregistrer l'activité d'un clavier</strong>.</li>
							<li>Vol des disques durs, et ainsi des <strong>toutes les données</strong> stockées.</li>
							<li>Changement de composants électroniques, possédant un <strong>microcode</strong> malveillant.</li>
							<li>Sabotages divers, tel que l'accès au réseaux, l'alimentation, ...</li>
						</ul>
						<img class="img-banner" alt="Salle sécurisé contenant des serveurs" src="../img/banner_server_04.jpg" />
					</section>
					<section>
						<h3>La sécurité du micro-processeur</h3>
						<p>Dans un ordinateur, le microprocesseur est l'<strong>unité de calcul</strong> servant à traiter les informations <strong>entrante</strong> afin de fournir des informations <strong>sortante</strong>.
						On appel cela en informatique le principe d'<strong>entrée/sortie</strong>, soit en anglais <em>input/output</em>, plus communément traduit par <em>I/O</em>
						Ainsi, le microprocesseur est composé de milliards de <a class="link" target="_blank" href="https://fr.wikipedia.org/wiki/Transistor">transistors</a>,
						de microscopiques composants électroniques permettant à l'ordinateur d'effectuer des milliards de calculs par seconde.</p>
						<p>C'est pourquoi l'existence d'une faille de sécurité dans le microprocesseur est un phénomène délicat.
						En effet, étant donné que <strong>toutes les informations</strong> transitent par le processeur, ou du moins dépendent des actions de ce dernier,
						se fier à un processeur défectueux <em>revient à conduire les yeux bandés en plein centre-ville à 130km/h</em>.
						Celui qui à le contrôle sur le processeur, a le contrôle sur <strong>toutes les actions</strong> du système d'information.
						Néanmois, certaines solutions sont possibles en cas de failles constatées :</p>
						<ul>
							<li>Des mises à jour logiciel sont le plus souvent disponibles, bien que pouvant dégrader les performances.</li>
							<li>Un changement de composant est envisageable, encore faut il que la compatibilté des composants accepte une famille de processeurs non-touchée par la faille en question.</li>
						</ul>
						<img class="img-banner" alt="Processeur d'un ordinateur" src="../img/banner_cpu_01.jpg" />
						<p>Récemment, c'est dans les dernières générations de processeurs <em>Intel</em> que des failles ont étés révélées, respectivement <em>Meltdown</em> et <em>Spectre</em>.
						Elles permettent entre autre de bénéficier d'une <strong>escalation des privilèges</strong> <em>-c'est à dire de béneficier du contrôle totale sur la machine-</em>
						et ainsi d'accéder au <strong>contenu de la mémoire</strong> système.
						Bien que les failles soit bel et bien réelles, aucune attaque informatique n'a été constatée, principalement dû à l'utilisation difficile de la faille.</p>
						<div class="content-row">
							<a class="link-img" target="_blank" href="https://meltdownattack.com/">
								<img class="img-banner-row" alt="Logo Metldown" src="../img/logo_meltdown.png" />
								<img class="img-banner-row" alt="Logo Spectre" src="../img/logo_spectre.png" />
							</a>
						</div>
					</section>
					<section>
						<h3>La sécurité du système d'exploitation</h3>
						<p>Le <strong>système d'exploitation</strong> est le logiciel permettant de faire le pont entre le matériel, et l'utilisateur.</p>
						<p>Parmi tous les systèmes d'exploitations disponibles, les plus utilisés sont ceux issus de <strong>GNU/Linux</strong>, inspirés eux-mêmes du système <em>UNIX</em>.
						On peut ainsi citer les <strong>distributions</strong> <em>GNU/Linux</em> les plus couramment utilisées dans les systèmes d'informations :</p>
						<ul>
							<li>Debian</li>
							<li>Ubuntu</li>
							<li>CentOS</li>
							<li>ArchLinux</li>
						</ul>
						<div class="content-row">
							<a class="link-img" target="_blank" href="https://www.gnu.org/">
								<img class="img-banner-row" alt="Logo GNU" src="../img/logo_gnu.jpg" />
							</a>
							<a class="link-img" target="_blank" href="https://www.kernel.org/">
								<img class="img-banner-row" alt="Logo Linux" src="../img/logo_linux.jpg" />
							</a>
							<a class="link-img" target="_blank" href="https://www.debian.org/">
								<img class="img-banner-row" atl="Logo Debian" src="../img/logo_debian.png" />
							</a>
						</div>
						<p>La sécurité du système d'exploitation est directement gérée par l'administrateur, dont le nom d'utilisateur est plus communément <strong>root</strong>.
						Dans les systèmes de type UNIX, chaque utilisateur et groupe ont des droits spécifiques selon chacun fichier.
						Cette séparation des droits permet d'encadrer les actions des utilisateurs, tout en <em>sécurisant les fichiers sensibles</em> :</p>
						<ul>
							<li>Droits <strong>d'exécution</strong> : l'utilisateur peut lancer un programme spécifique.</li>
							<li>Droits <strong>de lecture</strong> : l'utilisateur peut afficher le contenu d'un fichier spécifique.</li>
							<li>Droits <strong>d'écriture</strong> : l'utilisateur peut modifier un fichier spécifique.</li>
						</ul>
						<p>De plus, diverses modifications du <em>noyau Linux</em> permettent de restreindre davantage les accès aux informations sensibles.
						Par exemple, en 2000, la <em>NSA</em> à développé un outil, <a class="link" target="_blank" href="https://fr.wikipedia.org/wiki/SELinux">SELinux</a>,
						qui est maintenant devenu open-source et développé par <a class="link" target="_blank" href="https://www.redhat.com/">Red Hat</a>.</p>
						<p>Aussi, le chiffrement des disques contenant les informations sensibles est encouragé <em>-d'autant plus en cas d'intervention de sous-traitants sur les installations, ou en cas de vols-</em>.
						Le logiciel le plus courant se prénomme <a class="link" target="_blank" href="https://gitlab.com/cryptsetup/cryptsetup/blob/master/README.md">LUKS</a>, <em>-Linux Unified Key Setup-</em>, 
						permettant de chiffrer <em>tout type de volumes</em> (disques durs, clés usb, dvd, ...), ainsi que <em>tout types de partitions</em> (ext4, ext3, fat32, NTFS, ...).</p>
					</section>
					<section>
						<h3>La sécurité du réseau</h3>
						<p>En premier lieu, il faut s'assurer que les informations transmises à travers le réseau soient seulement lisibles par l'émetteur et le récepteur.
						Pour cela on utilise <strong>le chiffrement</strong>, plus couramment rencontré via le sigle <strong>https</strong> lorsque l'on navigue sur le web.
						Deux différents protocoles existent, par l'intermédiaire du logiciel <a class="link" target="_blank" href="https://www.openssl.org/">OpenSSL</a> : </p>
						<ul>
							<li><strong>TLS</strong> : ce protocole est aujourd'hui devenu la norme dans le chiffrement des communications.</li>
							<li><strong>SSL</strong> : bien qu'existant encore, ce protocole est aujourd'hui banni des standards, dû à de nombreuses failles de sécurité.</li>
						</ul>
						<img class="img-banner" alt="Salle sécurisé contenant des serveurs" src="../img/banner_server_02.jpg" />
						<p>La sécurité du réseau passe également par la gestion des <strong>ports</strong>, désignant l'interface logiciel connectant les logiciels exécuté sur une machine locale, à internet.
						Par définition, chaque port réseau est associé à un numéro et à un protocole d'échange de communication :</p>
						<ul>
							<li><strong>21</strong> : échange de fichier, FTP</li>
							<li><strong>22</strong> : connexions aux serveurs, SSH</li>
							<li><strong>25</strong> : envoi des mails, SMTP</li>
							<li><strong>53</strong> : résolutions de noms de domaine, DNS</li>
							<li><strong>80</strong> : connexion au web, HTTP</li>
							<li><strong>443</strong> : connexion chiffré au web, HTTPS</li>
						</ul>
						<p>Pour gérer quels ports doivent être <em>ouverts</em> ou <em>fermés</em> aux communications, on utilise les logiciels
						<a class="link" target="_blank" href="https://fr.wikipedia.org/wiki/Iptables">iptables</a> ou
						<a class="link" target="_blank" href="https://en.wikipedia.org/wiki/Uncomplicated_Firewall">ufw</a>, littéralement <em>Uncomplicated FireWall</em>, soit bien moins complexe à mettre en place que celui cité en premier.
						Ainsi, la méthode employée est de <strong>fermer tous les ports</strong> et de n'ouvrir que <strong>ceux nécessaires</strong>.
						</p>
					</section>
				</div>
				<hr class="title">
				<section>
					<h2>S'assurer de la sécurité</h2>
					<div class="content">
						<section>
							<h3>Les différents organismes</h3>
							<p>En France, deux principaux organismes sont rattachés à la sécurité informatique.</p>
							<p><strong>L'ANSSI</strong>, abréviation de <em>Agence Nationale de la Sécurité des Systèmes d'Informations</em>, a différents buts, tels que :</p>
							<ul>
								<li><strong>Conseiller</strong> l'Etat et ses administrations, ainsi que les entreprises.</li>
								<li><strong>Protéger les particuliers</strong> en menant des actions de sensibilisations.</li>
								<li>Permettre de <strong>déclarer des vulnérabilités</strong>, tout en préservant l'identité du dénonciateur.</li>
								<li>Informer sur les marches à suivre en cas d'attaque informatique.</li>
							</ul>
							<p><strong>La CNIL</strong>, abbréviation de <em>Commission Nationale de l"Informatique et des Libertés</em>, veille principalement aux tâches suivantes</p>
							<ul>
								<li><strong>Accompagner les professionels</strong> dans leur mise en conformité de la loi.</li>
								<li><strong>Aider les particuliers</strong> à maitriser leurs données personnelles.</li>
								<li><strong>Anticiper les dérives</strong> et <strong>innover des lois</strong>.</li>
								<li><strong>Contrôler et sanctionner</strong> les entreprises et organismes</li>
							</ul>
							<div class="content-row">
								<a class="link-img" target="_blank" href="https://www.ssi.gouv.fr/">
									<img class="img-banner-row" alt="Logo ANSSI" src="../img/logo_anssi.png" />
								</a>
								<a class="link-img" target="_blank" href="https://www.cnil.fr/">
									<img class="img-banner-row" alt="Logo Cnil" src="../img/logo_cnil.png" />
								</a>
							</div>
						</section>
						<section>
							<h3>Le pentest</h3>
							<p>Le <em>test d'intrustion</em>, soit <strong>pentest</strong> ou <em>penetration testing</em> en anglais, est une discipline informatique qui consiste à <em>analyser et attaquer un système d'information</em> afin de trouver des failles potentielles. 
							Cette attaque est menée par un professionel, au service de l'entreprise ou de l'organisme, et s'effectue de <em>manière légale</em>.</p>
							<img class="img-banner" alt="Salle sécurisé contenant des serveurs" src="../img/banner_pentest.jpg" />
							<p>Divers outils peuvent être employés, tels que :</p>
							<ul>
								<li><strong>Nmap</strong> : un scanner de <em>ports réseaux</em> permmettant entre autre de détecter les <em>différents services</em> d'un serveur.</li>
								<li><strong>Wireshark</strong> : un logiciel permettant de récupérer toutes les données passant par le réseau.</li>
								<li><strong>Metasploit</strong> : un <a class="link" target="_blank" href="https://fr.wikipedia.org/wiki/Framework#Description">framework</a> réputé,
								permettant de scanner et attaquer une cible, parmi une large quantité de failles connus.</li>
								<li><strong>KaliLinux</strong> : une distribution <em>GNU/Linux</em>, contenant les outils ci-dessus et bien d'autres, axée sur la pratique du pentest.</li>
							</ul>
							<div class="content-row">
								<a class="link-img" target="_blank" href="https://www.wireshark.org/">
									<img class="img-banner-row" alt="Logo WireShark" src="../img/logo_wireshark.png" />
								</a>
								<a class="link-img" target="_blank" href="https://www.metasploit.com/">
									<img class="img-banner-row" alt="Logo Metasploit" src="../img/logo_metasploit.png" />
								</a>
								<a class="link-img" target="_blank" href="https://www.kali.org/">
									<img class="img-banner-row" alt="Logo KaliLinux" src="../img/logo_kalilinux.png" />
								</a>
							</div>
						</section>
					</div>
				</section>
				<hr class="title">
			</section>
		</div>
	</body>
	<?php include "_footer.php" ?>
</html>

