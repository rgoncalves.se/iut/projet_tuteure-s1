<!DOCTYPE html>
<html lang="fr" dir="ltr">
	<head>
		<meta charset="utf-8">
		<title>La sécurité informatique - Interview</title>
		<link href="/style/style_base.css" rel="stylesheet" type="text/css" />
		<link href="/style/style_pages.css" rel="stylesheet" type="text/css" />
		<link rel="shortcut icon" type="image/png" href="/img/favicon.ico"/>
	</head>
	<?php include "_navbar.php" ?>
	<div class="backToTop">
		<a href="#" class="backToTop">
			▲
		</a>

	</div>
	<body>
		<div class="container">
			<section>
				<div class="sommaireTitle">
					<h3 class="sommaire">Sommaire</h3>
					<hr class="content">
					<div class="sommaireContent">
						<a class="sommaire" href="#dPresentation"><h4>Présentation</h4></a>
						<div class="sommaireContent" id="titre2">
							<a class="sommaire" href="#dPresentation"><h5>Frank BRIGNOLI</h5></a>
						</div>
						<div class="sommaireContent" id="titre2">
							<a class="sommaire" href="#dTradeMachines"><h5>TradeMachines</h5></a>
						</div>
						<a class="sommaire" href="#dRole"><h4>Son rôle</h4></a>
						<div class="sommaireContent" id="titre2" >
							<a class="sommaire" href="#dDirecteur"><h5>Directeur technique</h5></a>
							<a class="sommaire" href="#dStartegique"><h5>Le rôle stratégique</h5></a>
							<a class="sommaire" href="#dRecherche"><h5>La recherche et le développement</h5></a>
							<a class="sommaire" href="#dGestion"><h5>La gestion d'équipe</h5></a>
						</div>
						<a class="sommaire" href="#dParcours"><h4>Son parcours</h4></a>
						<div class="sommaireContent" id="titre2" >
							<a class="sommaire" href="#dEtudes"><h5>Ses études</h5></a>
							<a class="sommaire" href="#dIntegration"><h5>Son intégration professionnelle</h5></a>
						</div>
						<a class="sommaire" href="#dSecurite"><h4>La sécurité de l'entreprise</h4></a>
						<div class="sommaireContent" id="titre2" >
							<a class="sommaire" href="#dDonnees"><h5>Les données</h5></a>
							<a class="sommaire" href="#dMoyens"><h5>Les moyens mis en place</h5></a>
							<a class="sommaire" href="#dRgpd"><h5>Le RGPD</h5></a>
						</div>
					</div>
				</div>
				<h1>Frank BRIGNOLI</h1>
				<h2>Directeur technique chez TradeMachines</h2>
					<p>	Le 10 octobre 2019, nous avons eu la chance d'avoir été mis en contact avec Frank BRIGNOLI, dans le cadre de notre projet tuteuré.
					Ce dernier étant installé à Berlin (Allemagne), il nous a été impossible de le rencontrer physiquement pour cet entretien.</p>
					<p>Cependant, il a tout de même accepté de répondre à nos questions via internet.
					C'est alors que nous nous sommes mis à réaliser un questionnaire auquel il a répondu ce mardi 15 octobre.
					Voici ses réponses :</p>
			</section>
			<hr class="title">
			<section>
				<h2 id="dPresentation" >Présentation</h2>
				<div class="content">
					<p>Frank BRIGNOLI, âgé de 35 ans, est français mais vit et travaille à Berlin en Allemagne.
					Ses passions et centres d'intérêts sont la lecture, le sport (tels que la course à pied ou le vélo), les jeux vidéos, ainsi que les startups et les innovations : passions qu'il entretient via son travail.
					En effet, depuis février 2014 dans la startup Allemande: TradeMachines, il a le rôle de directeur technique (<a target="_blank" class="link" href="https://www.clementine.jobs/fiches-metiers/metiers-de-linformatique/metier-chief-technical-officer-cto/">CTO</a>: Chief Technology Officer). </p>
					<img class="img-banner" src="/img/banner_Brignoli.jpg" alt="Photo de Frank BRIGNOLI" style="width: 30em;">
					<p class="legende">Photographie de Frank BRIGNOLI disponible sur le site de TradeMachines</p>
					<section>
						<h3 id="dTradeMachines">TradeMachines</h3>
						<p>TradeMachines est en réalité un moteur de recherche de machines industrielles d'occasions fondé en octobre 2013,
						soit 4 mois avant l'intégration de Frank BRIGNOLI au sein de l'équipe où il tient un rôle majeur.</p>
						<div class="content-row">
							<a class="link-img" target="_blank" href="https://trademachines.fr/">
								<img class="img-banner-row" src="/img/banner_tmachines.png" alt="Logo de TradeMachines" />
							</a>
						</div>
						<img class="img-banner" src="/img/banner_tmachine_02.png" alt="Bannière de TradeMachines">
						<p class="legende"> Capture d'écran du site TradeMachines</p>
					</section>
				</div>
			</section>
			<hr class="content">
			<section>
				<h2 id="dRole">Son rôle</h2>
				<div class="content">
					<section>
						<h3 id="dDirecteur">Directeur technique</h3>
						<p>Pour Mr BRIGNOLI, la profession de directeur technique se définit de la manière suivante : <i>"Aligner la technologie avec la vision du business"</i>.
						Il n'a donc pas un rôle unique mais bien plusieurs qu'il mène au mieux.</p>
					</section>
					<section>
						<h3 id="dStartegique">Le rôle stratégique</h3>
						<p>Le premier est le rôle stratégique.
						En effet, il participe activement à la définition des objectifs de la société afin d'assurer son bon fonctionnement au long terme et de garder le cap sur l'avenir.
						Il doit également définir les nouveaux projets techniques que va aborder l'entreprise pour atteindre ses objectifs.</p>
					</section>
					<section>
						<h3 id="dRecherche">La recherche et le développement</h3>
						<p>Le second rôle se définit par la recherche et le développement.
						Travaillant dans une entreprise basée sur l'informatique, il se doit d'être à l'écoute des nouveautés techniques qui pourraient aider la société.
						De plus, il réalise des <strong>prototypes</strong> afin d'analyser et d'étudier ces nouvelles technologies.</p>
					</section>
					<section>
						<h3 id="dGestion">La gestion d'équipe</h3>
						<p>​Etant directeur technique, il a pour rôle la gestion d'équipe.
						Cela consiste en d'autres termes à structurer et à manager des employés afin d'en tirer le meilleur.
						Il doit donner la possibilité aux membres du groupe d'offrir leurs compétences les plus importantes pour l'avancer de la société.</p>
					</section>
					<img class="img-banner" src="/img/banner_meeting_01.jpg" alt="Photo d'un meeting">
					<p class="legende">Photographie illustrant la gestion d'équipe</p>
				</div>
			</section>
			<hr class="content">
			<section>
				<h2 id="dParcours">Son parcours</h2>
				<div class="content">
					<section>
						<h3 id="dEtudes">Ses études</h3>
						<p>Mr BRIGNOLI a obtenu un <a target="_blank" class="link" href="https://fr.wikipedia.org/wiki/Dipl%C3%B4me_universitaire_de_technologie_en_informatique"><strong>DUT</strong></a> (<em>Diplôme Universitaire de Technologie</em>) en informatique.
						Il a choisi cette formation car il s'agit d'études courtes et pragmatiques qui apportent un niveau technique important et donnant une bonne vue d'ensemble sur l'informatique.
						Son objectif était de devenir <a target="_blank" class="link" href="https://www.onisep.fr/Ressources/Univers-Metier/Metiers/developpeur-developpeuse-informatique"><strong>développeur</strong></a>,  mais sans vraiment penser à "l'après".
						Il voulait avant tout permettre d'influencer l'évolution d'une entreprise, notamment grâce à la <strong>technologie</strong>.</p>
						<img class="img-banner" src="/img/banner_meeting_02.jpg" alt="Photo de plusieurs développeurs">
						<p class="legende">Photographie illustrant le metier de développeur</p>
						<p><em>"En tant que développeur, on a une idée assez précise des nouvelles technologies qui existent et ce qu'elles font"</em> d'après lui.
						Son objectif était donc de passer du temps à réfléchir à leurs possibles impacts sur une entreprise.</p>
					</section>
					<section>
						<h3 id="dIntegration">Son intégration professionnelle</h3>
						<p>Par la suite, il a rejoint une grande <a target="_blank" class="link" href="https://fr.wikipedia.org/wiki/Entreprise_de_services_du_num%C3%A9rique"><strong>ESN</strong></a> (Entreprise de Services du Numérique) lui permettant d'intégrer l'entreprise de TradeMachines en Février 2014.
						Voici son parcours au sein de cette entreprise:</p>
						<ul>
							<li>Il a commencé tout d'abord en tant que <strong>développeur</strong></li>
							<li>1 an après, il s'est retrouvé <strong>architecte et opérateur</strong>.
								A ce moment là, son activité consistait à structurer le code et les services, puis de s'assurer qu'il n'y avait pas de discontinuité de ces services.</li>
							<li>En 2016, il est passé <strong>Lead équipe</strong> où sa mission était de coder tout en permettant à l'équipe de travailler sans difficulté et sans accroc</li>
							<li>C'est seulement 4 ans après avoir intégré TradMachines, soit en 2018, qu'il se retrouve directeur technique.</li>
						</ul>
						<p>Aujourd'hui, il est très épanoui dans son travail.
						Cette profession satisfait totalement ses attentes. </p>
					</section>
				</div>
			</section>
			<hr class="content">
			<section>
				<h2 id="dSecurite">La sécurité de l'entreprise</h2>
				<div class="content">
					<section>
						<h3 id="dDonnees">Les données</h3>
						<p>Pour Mr BRIGNOLI, la sécurité informatique est très importante pour trois raisons principales :</p>
						<ul>
							<li>Protéger les <a target="_blank" class="link" href="https://fr.wikipedia.org/wiki/Asset">assets</a> et la fiabilité de leurs services</li>
							<li>Protéger la réputation de l'entreprise vis à vis de la clientèle mais également face aux autres concurrents</li>
							<li>Protéger la vie privée de leurs utilisateurs.</li>
						</ul>
						<p>Ce dernier point est intéressant car TradeMachines, étant un site internet proposant des échanges commerciaux, est de ce fait très intéressé par les informations de contacts (email, adresse, téléphone).</p>
						<p>Il est donc nécessaire de protéger toutes ses données</p>
						<p>TradeMachines récupère les données de plus de <strong>1.150 vendeurs</strong> ainsi que de plus de <strong>520.000 visiteurs</strong> par mois.
						La protection se doit d'être irréprochable</p>
					</section>
					<section>
						<h3 id="dMoyens">Les moyens mis en place</h3>
						<p>Pour protégers toutes les données citées plus haut, TradeMachines met en place différents moyens tels que :</p>
						<ul>
							<li>Le <a target="_blank" class="link" href="https://fr.wikipedia.org/wiki/Chiffrement">chiffrement</a> de données sensibles</li>
							<li>La suppression des données automatiquement après un certain délai</li>
							<li>La restriction de l'accès aux données</li>
						</ul>
						<img class="img-banner" src="/img/banner_chiffrement_01.jpg" alt="Photo de chiffres">
						<p class="legende">Photographie illustrant le cryptage et le chiffrement des données</p>
						<p>Par la suite, il réalise également des <a target="_blank" class="link" href="https://fr.wikipedia.org/wiki/Audit_de_s%C3%A9curit%C3%A9">audits de sécurité</a> sur leurs serveurs et postes de travail.
						Cependant, il ne font pas appel à des sous-traitants pour la maintenance informatique ou encore pour la gestion des données.
						En effet, tout cela est réalisé en interne par le soin des professionnels</p>
						<p>Les serveurs de TradeMachines sont sous-traités dans le <a target="_blank" class="link" href="https://fr.wikipedia.org/wiki/Cloud_computing">cloud</a>.
						Cela leur permet de se concentrer sur le coeur du métier de leur <strong>startup</strong>.
						Mais également de bénéficier de services qu'ils ne pourraient pas avoir en interne sans y accoder un large budget tel que :</p>
						<ul>
							<li>Une sécurité physique des serveurs</li>
							<li>De multiples <a target="_blank" class="link" href="https://fr.wikipedia.org/wiki/Centre_de_donn%C3%A9es">data-center</a> proches de leurs clients</li>
							<li>Une large offres de services (parmi eux : une base de données, des serveurs, un stockage de fichiers, …)</li>
							<li>Une sécurité logicielle : comme des sauvegardes, des mises jours logiciels ou encore une sécurité réseau</li>
							<li>Une grande flexibilité avec la possibilité de changer le nombre et le type de serveurs en un seul click.</li>
						</ul>
					</section>
					<img class="img-banner" src="/img/banner_datacenter_01.jpg" alt="Photo de centre de données">
					<p class="legende">Photographie montrant des centres de données physiques</p>
					<section>
						<h3 id="dRgpd">Le RGPD</h3>
						<p>Le <a class="link" target="_blank" href="https://www.economie.gouv.fr/entreprises/reglement-general-sur-protection-des-donnees-rgpd"><strong>RGPD</strong></a> ou autrement dit le Règlement Général sur la Protection des Données, est un règlement de l'Union européenne qui constitue le texte de référence en matière de protection des données à caractère personnel.
						Il renforce et unifie la protection des données pour les individus au sein de l'Union européenne.
						Il est entré en vigueur le 14 avril 2016.</p>
						<p>Dans le cas de TradeMachines, n'ayant jamais eu de problèmes juridiques quels qu'ils soient, la venue du <strong>RGPD</strong> ne les a pas éffrayé.</p>
						<p>En effet leur réaction a simplement été de nommer un représentant interne afin d'analyser les changements nécessaires à réaliser.
						On peut citer par exemple les changements dans leur méthode de communication envers leurs utilisateurs (cookies, conservation des données, ...)
						ou encore dans leur rétention des données.</p>
					</section>
				</div>
			</section>
		</div>
	</body>
	<?php include "_footer.php" ?>
</html>
