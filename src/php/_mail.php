<?php
require_once "phpMailer/PHPMailer.php";
require_once "phpMailer/Exception.php";
require_once "phpMailer/SMTP.php";


use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

function showError() {
			echo '<div class="contact-hint">
				Message non-envoyé, veillez réessayer.
				</div>';
}

$mail = new PHPMailer(TRUE);
$gmail_mail = "ptuteure.securite@gmail.com";
$gmail_passwd = "Gm41lS8cks";
$gmail_name = "Gmail Account";

if(!empty($_POST['mail_subject']) AND !empty($_POST['mail_name']) AND !empty($_POST['mail_address']) AND !empty($_POST['mail_content'])) {
	try {
		// Init
		$mail->isSMTP();
		$mail->Host = "smtp.gmail.com";
		$mail->Port = 587;
		$mail->SMTPAuth = true;
		$mail->SMTPSecure = "tls";
		$mail->isHTML(false);
		// User settings
		$mail->Username = $gmail_mail;
		$mail->Password = $gmail_passwd;
		$mail->Subject = $_POST['mail_subject'];
		$mail->setFrom($_POST['mail_address'], $_POST['mail_name']);
		$mail->addAddress($gmail_mail, $gmail_name);
		//echo $_POST['mail_content'];
		$mail->Body = <<<EOT
		Email: {$_POST['mail_address']}
		Nom: {$_POST['mail_name']}
		Message: {$_POST['mail_content']}
EOT;
		if (!$mail->send()) {
			showError();
		} else {
			echo '<div class="contact-hint">
				Message envoyé !
				</div>';
		}
	}
	catch (Exception $e)
	{
		showError();
	}
}
else if(isset($_POST['mail_subject']) OR isset($_POST['mail_name']) OR isset($_POST['mail_address']) OR isset($_POST['mail_content'])) {
	showError();
}
