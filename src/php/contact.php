<!DOCTYPE html>
<html lang="fr" dir="ltr">
	<head>
		<meta charset="utf-8">
		<title>La sécurité informatique - Contact</title>
		<link href="/style/style_base.css" rel="stylesheet" type="text/css" />
		<link href="/style/style_pages.css" rel="stylesheet" type="text/css" />
		<link rel="shortcut icon" type="image/png" href="/img/favicon.ico"/>
	</head>
	<?php include "_navbar.php"; ?>
	<?php include "_mail.php"; ?>
	<body>
		<div class="container">
			<section>
				<h2>Nous contacter</h2>
				<div class="content">
					<form action="" method="post">
						<label for="mail_subject">Objet :</label>
						<input type="text" name="mail_subject">
						<br>
						<label for="mail_name">Nom :</label>
						<input type="text" name="mail_name">
						<br>
						<label for="mail_address">Addresse Mail :</label>
						<input type="text" name="mail_address">
						<br>
						<label for="mail_content">Contenu :</label>
						<textarea name="mail_content"></textarea>
						<button type="submit">Envoyer</button>
					</form>
				</div>
			</section>
		</div>
	</body>
	<?php include "_footer.php" ?>
</html>

