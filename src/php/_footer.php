<link href="/style/style_footer.css" rel="stylesheet" type="text/css" />
<footer class="magnetic">
	<div id="footer-members">
		Site réalisé par:
		<ul class="footer-ul">
			<li class="footer-li">DUBOIS Evan</li>
			<li class="footer-li">GONCALVES Romain</li>
			<li class="footer-li">GOYARD Hugo</li>
			<li class="footer-li">PEPIN Maxime</li>
			<li class="footer-li">SCHMELTZ Arnaud</li>
		</ul>
	</div>
	<div id="footer-thanks">
		Remerciements:
		<ul class="footer-ul">
			<li class="footer-li">BRIGNOLI Franck</li>
			<li class="footer-li">PATERLINI Corinne</li>
			<li class="footer-li">SAHLER Thierry</li>
			<li class="footer-li">TRANNOY David</li>
		</ul>
	</div>
	<div id="footer-logo">
		<a href="https://www.iut-bm.univ-fcomte.fr/" target="_blank">
			<img id="footer-logo-img" src="/img/icon_iutbm.png" alt="Logo IUTBM" />
		</a>
	</div>
</footer>
