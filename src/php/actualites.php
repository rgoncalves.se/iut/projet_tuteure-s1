<!DOCTYPE html>
<html lang="fr" dir="ltr">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content_collapse="width=device-width, initial-scale=1"> 
		<title>La sécurité informatique - Présentation</title>
		<link href="style/style_pages.css" rel="stylesheet" type="text/css"/>
		<link rel="shortcut icon" type="image/png" href="img/favicon.ico"/>
		<link href="/style/style_pages.css" rel="stylesheet" type="text/css" />
		<link rel="shortcut icon" type="image/png" href="/img/favicon.ico"/>
	</head>
	<?php include "_navbar.php" ?>
	<body>
		<div class="container">
			<section>
				<h1>Actualités</h1>
				<p><strong>Retrouvez ici les actualités marquantes de ces dernières années, liées à la sécurité informatique</strong></p>
			</section>
			<hr class="title">
			<section>
				<div class="container">
					<button class="accordion">WannaCry</button>
					<div class="panel">
					  <p>Considérée comme la plus grande <a class="link" target="_blank" href="https://fr.wikipedia.org/wiki/Cyberattaque">cyberattaque</a> mondiale massive, touchant plus de 300 000 ordinateurs dans plus de 150 pays. Elle à été réalisée en utilisant le système obsolète de Windows XP mais également toutes les versions antérieures à Windows 10 n'ayant pas effectué les mises à jour de sécurité.</p>
					  <img class="img-banner" style="width: 40%" alt="Interface de WannaCry" src="../img/banner_wannacry.png" />
					  <p class="legende">Interface graphique du logiciel malveillant WannaCry</p>
					  <p>Apparu en 2017, WannaCry est un logiciel malveillant (<a class="link" target="_blank" href="https://fr.wikipedia.org/wiki/Ran%C3%A7ongiciel">ransomware</a>), celui-ci encryptait les données et demandait une rançon en échange, si la rançon n'était pas réalisée au bout d'un certain temps imposé, les informations seraient supprimer du système opérant.</p>
					</div>
					<button class="accordion">Cyberattaque de 2016 contre DynDNS</button>
					<div class="panel">
					 	<p>En octobre 2016, DynsDNS, l'entreprise la plus utilisée au monde pour la résolution des noms de domaines en adresses IP, subit une lourde attaque informatique.
					 	Cette attaque par <a class="link" target="_blank" href="https://fr.wikipedia.org/w/index.php?title=Deni_de_service">déni de service</a>, consistant à envoyer des centaines de milliers de requêtes TCP/IP par secondes, rend les services de DynDNS inutilisables durant plus d'une demi-journée, laissant des centaines millions d'utilisateurs dans l'impossibilité d'accéder à leurs sites préférés.</p>
						<img class="img-banner" style="" alt="Interface de WannaCry" src="../img/banner_dyndns.png" />
						<p class="legende">Localisation des sites touchés par l'attaque sur le service DynDNS</p>
						<p>Aujourd'hui, le code source du logiciel Mirai est disponible sur la plateforme Github.
						Le logiciel consistait à infecter des appareils connectés tels que des caméras, puis de les réunir en une sorte d'armée afin de rediriger leurs requêtes de connexions sur le service DynDNS.</p>
						<div class="content-row">
							<a class="link-img" target="_blank" href="https://github.com/jgamblin/Mirai-Source-Code/">
								<img class="img-banner-row" alt="Logo Github" src="../img/logo_github.png" />
							</a>
						</div>
					</div>
					<button class="accordion">Cambridge Analytica</button>
					<div class="panel">
					 	<p>Entreprise spécialisée dans la politique américaine, <strong>Cambridge</strong> Analytica voit sa disparition en 2018.
					 	En effet, cette dernière à oeuvré dans les élections présidentielles américaines de 2016, et le référendum sur le Brexit.
						Dans les deux cas, Cambridge Analytica a joué un rôle majeur dans la collecte et le traitement de données de dizaines de <strong>millions d'utilisateurs de Facebook</strong>.</p>
						<div class="content-row">
							<a class="link-img" target="_blank" href="https://web.archive.org/web/20180321032242/https://cambridgeanalytica.org//">
								<img class="img-banner-row" alt="Logo Github" src="../img/logo_cambridgeanalytica.jpg" />
							</a>
						</div>
						<p>Juridiquement parlant, la collaboration entre Facebook et Cambridge Analytica prouve que Facebook n'a su protéger les données de ses utilisateurs, et a encouru des poursuites, que ce soit aux Etats-Unis et en Europe.
						En effet, on estime que l'ingérence de Cambridge Analytica dans les élections a participé à <strong>tromper les électeurs</strong>, et à la diffusion de fake news.</p>
					</div>
					<button class="accordion">Edward Snowden</button>
					<div class="panel">
					 	<p>Edward Snowden, ancien informaticien au compte de la NSA et de la CIA, est aujourd'hui un défenseur de la vie privée et de l'Internet libre.
					  	En effet, en 2013, il a révélé au monde entier que les agences de renseignement américaines et leurs alliés récoltent les données, et les faits et gestes de tous les utilisateurs sur internet.
					 	</p>
						<img class="img-banner" style="width: 40%" alt="Photo Edward Snowden" src="../img/logo_snowden.jpg" />
						<p>Les programmes de surveillance dénoncés sont par exemple :</p>
						<ul>
							<li><em>PRISM</em></li>
							<li><em>XKeyScore</em></li>
							<li><em>Tempora</em></li>
						</ul>
						<p>Entre autres, la fiabilité des services de renseignement est aujourd'hui à remettre en questions.
						D'autant plus que pour peu que ces agences trouvent des failles de sécurité majeures dans les systèmes d'information, elles se gardent de les faire remonter aux développeurs, afin d'en profiter, mais en mettant en danger des centaines de millions d'utilisateurs.</p>
					</div>
				</div>
			</section>
			<hr class="title">
		</div>
	</body>
	<?php include "_footer.php" ?>
	<script src="/js/collapse.js"></script>
</html>
